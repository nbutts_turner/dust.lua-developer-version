#!/usr/bin/env node

var dust = require( 'dustjs-linkedin' ),
	fs = require('fs');

var introSource = "dust = require 'dust'\n" + dust.compile( '{#stream}{#delay}{.}{/delay}{/stream}', 'intro' );
var introFileName = 'compiled/demo_intro.lua';
fs.open( introFileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( introSource ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				// if ( _err ) { throw _err; }
				console.log( 'wrote ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );

// combo
var sourceCombo = "dust = require 'dust'\n" + dust.compile( 'Hello {name}! Followed by section {#names}{title} {name}{~n}{/names}', 'combo' );
fileName = 'compiled/demo_combo.lua';

fs.writeFile( fileName, sourceCombo, function( _err ) { 
        if ( _err ) { 
                console.log( _err );
        } else { 
                console.log( "The file (" + fileName + ") was saved" );
        }
} );


// plain
var sourcePlain = "dust = require 'dust'\n" + dust.compile( 'Hello World!', 'plain' );
fileName = 'compiled/demo_plain.lua';

fs.writeFile( fileName, sourcePlain, function( _err ) { 
        if ( _err ) { 
                console.log( _err );
        } else { 
                console.log( "The file (" + fileName + ") was saved" );
        }
} );


// replace
var source = "dust = require 'dust'\n" + dust.compile( 'Hello {name}! You have {count} new messages.', 'replace' );
var fileName = 'compiled/demo_replace.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( source ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				// if ( _err ) { throw _err; }
				console.log( 'wrote ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


// array
var sourceArray = "dust = require 'dust'\n" + dust.compile( '{#names}{title} {name}{~n}{/names}', 'array' );
var fileName = 'compiled/demo_array.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceArray ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


var sourceImplicit = "dust = require 'dust'\n" + dust.compile( '{#names}{.}{~n}{/names}', 'implicit' );
var fileName = 'compiled/demo_implicit.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceImplicit ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


var sourceObject = "dust = require 'dust'\n" + dust.compile( '{#person}{root}: {name}, {age}{/person}', 'object' );
var fileName = 'compiled/demo_object.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceObject ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );

var sourcePath = "dust = require 'dust'\n" + dust.compile( '{foo.bar}ada adfdasd', 'path' );
var fileName = 'compiled/demo_path.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourcePath ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


var sourceRenameKey = "dust = require 'dust'\n" + dust.compile( '{#person foo=root}{foo}: {name}, {age}{/person}', 'rename_key' );
var fileName = 'compiled/demo_renamekey.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceRenameKey ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


var sourceForceCurrent = "dust = require 'dust'\n" + dust.compile( '{#person}{.root}:{name}, {age}{/person} JAH', 'force_current' );
var fileName = 'compiled/demo_forcecurrent.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceForceCurrent ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );



var sourceEscaped = "dust = require 'dust'\n" + dust.compile( '{safe|s}{~n}{unsafe}', 'escaped' );
var fileName = 'compiled/demo_escaped.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceEscaped ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );

var sourceEscapedPragma = "dust = require 'dust'\n" + dust.compile( '{%esc:s}{unsafe}{~n}{%esc:h}{unsafe}{/esc}{/esc}', 'escaped_pragma' );
var fileName = 'compiled/demo_escapedpragma.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceEscapedPragma ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );

var sourceElse = "dust = require 'dust'\n" + dust.compile( '{#foo}foo,{~s}{:otherwise}not foo,{~s}{/foo}{#bar}bar!{:otherwise}not bar!{/bar}', 'else_block' );
var fileName = 'compiled/demo_else.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceElse ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


var sourceConditional = "dust = require 'dust'\n" + dust.compile( '{?tags}<ul>{~n}{#tags}{~s} <li>{.}</li>{~n}{/tags} </ul>{:otherwise} No Tags!{/tags}{~n}{^likes} No Likes!{:otherwise}<ul>{~n}{#likes}{~s} <li>{.}</li>{~n}{/likes}</ul>{/likes}', 'conditional' );
var fileName = 'compiled/demo_conditional.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceConditional ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );


var sourceSyncKey = "dust = require 'dust'\n" + dust.compile( 'Hello {type} World!', 'sync_key' );
var fileName = 'compiled/demo_synckey.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceSyncKey ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );

var sourceSyncChunk = "dust = require 'dust'\n" + dust.compile( 'Hello {type} World!', 'sync_chunk' );
var fileName = 'compiled/demo_syncchunk.lua';
fs.open( fileName, 'w', 
	function( _err, _fd ) {
		if ( _err ) { throw _err; }

		var writeBuffer = new Buffer( sourceSyncChunk ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;

		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, 
			function( _err, _written ) {
				if ( _err ) { throw _err }
				console.log( 'wrote  (' + fileName + ') of size: ' + _written + ' bytes ' );
			} 
		);

		fs.close( _fd );
 	}
 );

var sourceAsyncIterator = "dust = require 'dust'\n" + dust.compile( '{#numbers}{#delay}{.}{/delay}{@sep}, {/sep}{/numbers}', 'async_iterator' );
var fileName = 'compiled/demo_asynciterator.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceAsyncIterator ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);



var sourceFilter = "dust = require 'dust'\n" + dust.compile( '{#filter}foo {bar}{/filter}', 'filter' );
var fileName = 'compiled/demo_filter.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceFilter ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


var sourceContext = "dust = require 'dust'\n" + dust.compile( '{#list:projects}{name}{:otherwise}No Projects!{/list}', 'context' );
var fileName = 'compiled/demo_context.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceContext ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


// Params
var sourceParam = "dust = require 'dust'\n" + dust.compile( '{#helper foo="bar" boo="far"/}', 'param' );
var fileName = 'compiled/demo_param.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceParam ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


// Partials
var sourcePartials = "dust = require 'dust'\n" + dust.compile( '{>replace/} {>"plain"/} {>"{ref}"/}', 'partials' );
var fileName = 'compiled/demo_partials.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourcePartials ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);

// Partial Context
var sourcePartialContext = "dust = require 'dust'\n" + dust.compile( '{>replace:.profile/}', 'partial_context' );
var fileName = 'compiled/demo_partialcontext.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourcePartialContext ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


// Base Template
var sourceBaseTemplate = "dust = require 'dust'\n" + dust.compile( 'Start{~n}{+title} Base Title {/title}{~n}{+main} Base Content {/main}{~n} End', 'base_template' );
var fileName = 'compiled/demo_basetemplate.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceBaseTemplate ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


// Child Template
var sourceChildTemplate = "dust = require 'dust'\n" + dust.compile( '{^xhr}{>base_template/}{:otherwise}{+main/}{/xhr}{<title}Child Title{/title}{<main}Child Content{/main}', 'child_template' );
var fileName = 'compiled/demo_childtemplate.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceChildTemplate ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


// Recursion
var sourceRecursion = "dust = require 'dust'\n" + dust.compile( '{name}{~n}{#kids}{>recursion:./}{/kids}', 'recursion' );
var fileName = 'compiled/demo_recursion.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceRecursion ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);


// Comment
var sourceComment = "dust = require 'dust'\n" + dust.compile( '{! Multiline	{#foo}{bar}{/foo} !} {!before!}Hello{!after!}', 'comment' );
var fileName = 'compiled/demo_comment.lua';
fs.open( fileName, 'w',
	function( _err, _fd ) {
		if ( _err ) { throw _err; }
		var writeBuffer = new Buffer( sourceComment ), bufferOffset = 0, bufferLength = writeBuffer.length, filePosition = null;
		fs.write( _fd, writeBuffer, bufferOffset, bufferLength, filePosition, function( _err, _written ) {
			if ( _err ) {
				throw _err;
			}
			console.log( 'wrote (' + fileName + ') of size: ' + _written + ' bytes ' );

		} );
		fs.close( _fd );
	}
);







