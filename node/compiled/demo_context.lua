dust = require 'dust'
local M = {}
function M.register()
 dust.register( "context",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx:get( "list" ),ctx:rebase(ctx:get( "projects" )),{otherwise=M.body_1,block=M.body_2},nil)
end
 
function M.body_1(chk,ctx)
return chk:write( "No Projects!")
end
 
function M.body_2(chk,ctx)
return chk:reference(ctx:get( "name" ),ctx,"h")
end
 
return M
