dust = require 'dust'
local M = {}
function M.register()
 dust.register( "object",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx:get( "person" ),ctx,{block=M.body_1},nil)
end
 
function M.body_1(chk,ctx)
return chk:reference(ctx:get( "root" ),ctx,"h"):write( ": "):reference(ctx:get( "name" ),ctx,"h"):write( ", "):reference(ctx:get( "age" ),ctx,"h")
end
 
return M
