dust = require 'dust'
local M = {}
function M.register()
 dust.register( "partials",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:partial('replace',ctx,nil):write( " "):partial('plain',ctx,nil):write( " "):partial('M.body_1',ctx,nil)
end
 
function M.body_1(chk,ctx)
return chk:reference(ctx:get( "ref" ),ctx,"h")
end
 
return M
