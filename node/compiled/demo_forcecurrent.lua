dust = require 'dust'
local M = {}
function M.register()
 dust.register( "force_current",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx:get( "person" ),ctx,{block=M.body_1},nil):write( " JAH")
end
 
function M.body_1(chk,ctx)
return chk:reference( ctx:getPath( true,{ "root" } ),ctx,"h"):write( ":"):reference(ctx:get( "name" ),ctx,"h"):write( ", "):reference(ctx:get( "age" ),ctx,"h")
end
 
return M
