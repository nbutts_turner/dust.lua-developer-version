dust = require 'dust'
local M = {}
function M.register()
 dust.register( "child_template",M.body_0); 
end
local blocks={title=M.body_3,main=M.body_4}
function M.body_0(chk,ctx)
ctx=ctx:shiftBlocks(blocks)
return chk:notexists(ctx:get( "xhr" ),ctx,{otherwise=M.body_1,block=M.body_2},nil)
end
 
function M.body_1(chk,ctx)
ctx=ctx:shiftBlocks(blocks)
return chk:block(ctx:getBlock("main"),ctx,{},nil)
end
 
function M.body_2(chk,ctx)
ctx=ctx:shiftBlocks(blocks)
return chk:partial('base_template',ctx,nil)
end
 
function M.body_3(chk,ctx)
ctx=ctx:shiftBlocks(blocks)
return chk:write( "Child Title")
end
 
function M.body_4(chk,ctx)
ctx=ctx:shiftBlocks(blocks)
return chk:write( "Child Content")
end
 
return M
