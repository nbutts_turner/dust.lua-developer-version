dust = reuire 'dust'
local M = {}
function M.register()
 dust.register( nil,M.body_0); 
end
function M.body_0(chk,ctx)
return chk.section( chk, ctx.get( "stream" ),ctx,{block=M.body_1},nil)
end
 
function M.body_1(chk,ctx)
return chk.section( chk, ctx.get( "delay" ),ctx,{block=M.body_2},nil)
end
 
function M.body_2(chk,ctx)
return chk.reference( chk, ctx.getPath( true,{  } ),ctx,"h")
end
 
return M
