dust = require 'dust'
local M = {}
function M.register()
 dust.register( "implicit",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx:get( "names" ),ctx,{block=M.body_1},nil)
end
 
function M.body_1(chk,ctx)
return chk:reference( ctx:getPath( true,{  } ),ctx,"h"):write( "\n")
end
 
return M
