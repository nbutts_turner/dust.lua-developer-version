dust = require 'dust'
local M = {}
function M.register()
 dust.register( "filter",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx:get( "filter" ),ctx,{block=M.body_1},nil)
end
 
function M.body_1(chk,ctx)
return chk:write( "foo "):reference(ctx:get( "bar" ),ctx,"h")
end
 
return M
