dust = require 'dust'
local M = {}
function M.register()
 dust.register( "escaped",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:reference(ctx:get( "safe" ),ctx,"h",{"s"}):write( "\n"):reference(ctx:get( "unsafe" ),ctx,"h")
end
 
return M
