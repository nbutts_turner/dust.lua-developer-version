dust = require 'dust'
local M = {}
function M.register()
 dust.register( "escaped_pragma",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:reference(ctx:get( "unsafe" ),ctx,""):write( "\n"):reference(ctx:get( "unsafe" ),ctx,"h")
end
 
return M
