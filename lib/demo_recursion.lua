dust = require 'dust'
local M = {}
function M.register()
 dust.register( "recursion",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:reference(ctx:get( "name" ),ctx,"h"):write( "\n"):section(ctx:get( "kids" ),ctx,{block=M.body_1},nil)
end
 
function M.body_1(chk,ctx)
return chk:partial('recursion',ctx:rebase( ctx:getPath( true,{  } )),nil)
end
 
return M
