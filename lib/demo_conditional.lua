dust = require 'dust'
local M = {}
function M.register()
 dust.register( "conditional",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:exists(ctx.get( "tags" ),ctx,{otherwise=M.body_1,block=M.body_2},nil):write( "\n"):notexists(ctx.get( "likes" ),ctx,{otherwise=M.body_4,block=M.body_6},nil)
end
 
function M.body_1(chk,ctx)
return chk:write( " No Tags!")
end
 
function M.body_2(chk,ctx)
return chk:write( "<ul>\n"):section(ctx.get( "tags" ),ctx,{block=M.body_3},nil):write( " </ul>")
end
 
function M.body_3(chk,ctx)
return chk:write( "  <li>"):reference( ctx.getPath( true,{  } ),ctx,"h"):write( "</li>\n")
end
 
function M.body_4(chk,ctx)
return chk:write( "<ul>\n"):section(ctx.get( "likes" ),ctx,{block=M.body_5},nil):write( "</ul>")
end
 
function M.body_5(chk,ctx)
return chk:write( "  <li>"):reference( ctx.getPath( true,{  } ),ctx,"h"):write( "</li>\n")
end
 
function M.body_6(chk,ctx)
return chk:write( " No Likes!")
end
 
return M
