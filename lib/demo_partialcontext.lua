dust = require 'dust'
local M = {}
function M.register()
 dust.register( "partial_context",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:partial("replace",ctx.rebase( ctx.getPath( true,{ "profile" } )))
end
 
return M
