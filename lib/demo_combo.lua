dust = require 'dust'
local M = {}
function M.register()
 dust.register( "combo",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:write( "Hello "):reference(ctx.get( "name" ),ctx,"h"):write( "! Followed by section "):section(ctx.get( "names" ),ctx,{block=M.body_2},nil)
end
 
function M.body_1(chk,ctx)
return chk:reference(ctx.get( "title" ),ctx,"h"):write( " "):reference(ctx.get( "name" ),ctx,"h"):write( "\n")
end
 
function M.body_2(chk,ctx)
return chk:reference(ctx.get( "title" ),ctx,"h"):write( " "):reference(ctx.get( "name" ),ctx,"h"):write( "\n")
end
 
return M
