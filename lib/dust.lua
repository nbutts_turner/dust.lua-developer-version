local Stub = require 'dust.stub'
local Context = require 'dust.Context'
local Stream = require 'dust.Stream'
local helpers = require 'dust.helpers'

local dust = {}
dust.cache = {}

dust.register = function( name, tmpl )
  if not name then return end
  dust.cache[name] = tmpl
end

-- Setup to make the Streaming demo work.  Have some problems getting the actual values however.
-- Getting a nested table of blank values and need to investigate more.
-- Stream does not work.
dust.renderDemo = function( name, context, callback )
  print( 'dust.renderDemo()' )
    if ( type( context ) == 'function' ) then
      context = context()
    end
    returnVal = dust.stream( name, context )
    if ( type( returnVal ) == 'table' ) then
      print( returnVal.head.data )
      for k,v in pairs( returnVal.head.root.head.root.head.root.head.root.head ) do
        print( "renderDemo Output-=>" .. k,v )
      end
    end
    
end

dust.render = function( name, context, callback )
  local chunk = Stub.new( callback ).head
  local returnVal = dust.load( name, chunk, Context.wrap( context ) ) -- [[ .end() ]]
  returnVal.finish( returnVal )
end

dust.stream = function( name, context )
  local stream = Stream.new()
  dust.nextTick( function() 
    dust.load(name, stream.head, Context.wrap(context)) -- [[.end() ]]
  end )
  return stream
end

dust.renderSource = function( source, context, callback )
  return dust.compileFn( source )( context, callback )
end

dust.compileFn = function( source, name )
  local tmpl = dust.loadSource( dust.compile( source, name ) )
  return function( context, callback )
    local master = ( callback and Stub.new( callback ) ) or Stream.new()
      dust.nextTick( function()
      tmpl(master.head, Context.wrap(context)).finish() --[[.end()]]
    end )
    return master
  end
end

dust.load = function( name, chunk, context )
    local tmpl = dust.cache[name]
  
    if tmpl then 
      return tmpl( chunk, context )
    elseif dust.onLoad then
        return chunk.map( function( chunk )
                dust.onLoad( name, function( err, src )
          if ( err ) then return chunk:setError( err ) end
          if ( not dust.cache[name] ) then dust.loadSource( dust.compile( src, name ) ) end
          dust.cache[name]( chunk, context ).finish() --[[.end()]]
      end )
    end )
  else
    return chunk:setError( 'template not Found' ) -- new Error( "Template Not Found: " + name ) )
  end
end

dust.loadSource = function( source, path )
  return eval( source )
end

dust.nextTick = function( callback )
  callback() -- setTimeout( callback, 0 ) --[[ need the setTimeout equivalent for Lua or just call it directly without delay. nabcorrection ]]--
end

dust.filter = function( string, auto, filters )
  if ( filters ) then
    for i = 1, #filters do
      local name = filters[i];
      if ( name == "s" ) then auto = null
      else string = dust.filters[name](string)
      end
    end
  end
  if ( auto ) then string = dust.filters[auto](string) end
  return string;
end

dust.filters = {}
dust.filters["u"] = encodeURI
dust.filters["uc"] = encodeURIComponent

dust.filters["h"] = function( value ) 
  return dust.escapeHtml(value)
end

dust.filters["j"] = function( value ) 
  return dust.escapeJs(value)
end

dust.makeBase = function( global )
  return --[[new Context(new Stack(), global) ]]
end

---Checks if a table is used as an array. That is: the keys start with one and are sequential numbers
-- @param t table
-- @return nil,error string if t is not a table
-- @return true/false if t is an array/isn't an array
-- NOTE: it returns true for an empty table
-- From: http://stackoverflow.com/questions/7526223/how-do-i-know-if-a-table-is-an-array
dust.isArray = function(t)
    if type(t)~="table" then return nil,"Argument is not a table! It is: "..type(t) end
    --check if all the table keys are numerical and count their number
    local count=0
    for k,v in pairs(t) do
        if type(k)~="number" then return false else count=count+1 end
    end
    --all keys are numerical. now let's see if they are sequential and start with 1
    for i=1,count do
        --Hint: the VALUE might be "nil", in that case "not t[i]" isn't enough, that's why we check the type
        if not t[i] and type(t[i])~="nil" then return false end
    end
    return true
end

local HCHARS = '[&<>\"]'
dust.escapeHtml = function( s ) 
  if ( type( s ) == 'string' ) then
    if ( string.find( s, HCHARS ) == nil ) then
      return s;
    else 
      return string.gsub( s, '%&', '&amp;' ).gsub( s, '%<', '&lt' ).gsub( s, '%>', '&gt;' ).gsub( s, '%"', '&quot;' )
    end
  end

  return s
end

dust.escapeJs = function( s ) 
  if ( type( s ) == 'string' ) then
    return string.gsub( s, '\\', '\\' ).gsub( s, '\r', '\\r' ).gsub( s, '"', '\\"' ).gsub( s, "'", "\\'") -- .gsub( s, '\u2028', '\\u2028' ).gsub( s, '\u2029', '\\u2029' ).gsub( s, '\n', '\\n' ).gsub( s, '\f', '\\f' ).gsub( s, '\t', '\\t' )
  else
    return s
  end
end

return dust;