local Tap = {}

Tap.new = function( head, tail )
	local this = {
		head = head,
		tail = tail
	}

	local push = function( tap ) 
		return Tap.new( tap, self )
	end

	local go = function( value )
		local tap = self

		while( tap ) do
			value = tap.head( value )
			tap = tap.tail
		end
	end

    local getTail = function()
        return this.tail
    end

    local getHead = function()
        return this.head
    end

	return {
		push = push,
		go = go,
        getTail = getTail,
        getHead = getHead
	}

end

return Tap