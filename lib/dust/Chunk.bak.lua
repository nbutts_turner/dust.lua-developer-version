local dust = require "dust.helpers"
local instanceof = dust.instanceof

local Chunk = {}
Chunk.prototype = {}

Chunk.new = function( root, _next, taps )
    local this = {
		root = root,
		next = _next,
		data = '',
		flushable = false,
		taps = taps
    }
    print( 'in Chunk.new() -=> this.flushable -=> ' .. tostring( this.flushable ) )
    return setmetatable( this, { __index = Chunk.prototype } )
end

function Chunk.prototype:write( data )
    local taps = self.taps
    
    if (taps) then data = taps.go(data) end
    self.data = self.data .. tostring( data )
    return self
end

Chunk.prototype.finish = function(this, data) 
    if (data) then
		this:write(data)
    end
    this.flushable = true
    this.root:flush(this.root)
    return this
end

Chunk.prototype.map = function(this, callback) 
	print( 'Chunk.prototype.map() called' )
    local cursor = Chunk.new(this.root, this.next, this.taps)
    local branch = Chunk.new(this.root, cursor, this.taps)
    
    this.next = branch
    this.flushable = true
    callback(branch)
    return cursor
end

Chunk.prototype.tap = function(this, tap)
    local taps = this.taps
    
    if (taps) then
	this.taps = taps:push(tap)
    else 
	this.taps = Tap.new(tap)
    end
    return this
end

Chunk.prototype.untap = function(this)
    this.taps = this.taps.tail
    return this
end

Chunk.prototype.render = function(this, body, context)
    return body(this, context)
end

Chunk.prototype.reference = function(this, elem, context, auto, filters) 
    if (type(elem) == "function") then
		elem = elem(this, context, nil, {auto = auto, filters = filters})
		if (instanceof(elem, Chunk)) then
	    	return elem
		end
    end
    if (not dust.isEmpty(elem)) then
    	print( 'Chunk.prototype.reference()-=>filters-type-=>' .. tostring( type( filters ) ) )
		return this:write(dust.filter(elem, auto, filters))
    else
		return this
    end
end

Chunk.prototype.section = function(this, elem, context, bodies, params) 
	print( 'Chunk.prototype.section() -=> type(elem) -=> ' .. type(elem))
	print( 'Chunk.prototype.section() -=> elem value -=>  "' .. tostring( elem ) .. '"'	)
    if (type(elem) == "function") then
    	print( 'Chunk.prototype.section()-=>type(elem)=function' )
		elem = elem(this, context, bodies, params)
		if (instanceof(elem, Chunk)) then
	    	return elem
		end
    end

    local body = bodies.block
    local skip = bodies['else']

    if ( params ) then
    	context = context.push( params )
	end

	if (dust.isArray(elem)) then
	    if (body) then
			local len = #elem
			local chunk = this
			for i=1, len do
		    	chunk = body(chunk, context.push(elem[i], i, len))
			end
			return chunk
		end
	elseif (elem == true) then
		if (body) then 
			return body(this, context)
		end
	elseif ( elem or elem == 0 ) then
		print( 'Chunk.prototype.section()-=>in elem or elem == 0 elseif block ' .. type( elem ) )
	    if ( body ) then return body(this, context.push(elem)) end
	elseif (skip) then return skip(this, context) 
	else
		print( 'Chunk.prototype.section() -=> ' .. type( elem ) )
	end

    return this;
end

Chunk.prototype.exists = function(elem, context, bodies) 
	local body = bodies.block
	local skip = bodies['else']

	if (not dust.isEmpty(elem)) then
	    if (body) then return body(this, context)
		elseif (skip) then
			return skip(this, context)
		end
	    return this;
	end
end

Chunk.prototype.notexists = function(elem, context, bodies)
	local body = bodies.block
	local skip = bodies['else']

	if (dust.isEmpty(elem)) then
	    if (body) then return body(this, context)
		elseif (skip) then return skip(this, context) end
	    return this
	 end
end

Chunk.prototype.block = function(elem, context, bodies) 
	print( 'Chunk.prototype.block() called' )
	local body = bodies.block

	if (elem) then body = elem end
	if (body) then return body(this, context) end
	return this
end

Chunk.prototype.partial = function(elem, context)
	if ( type( elem ) == "function" ) then
		return this.capture(elem, context, 
				function(name, chunk) 
				    dust.load(name, chunk, context).finish();
				end )
	end
	return dust.load(elem, this, context)
end

Chunk.prototype.helper = function(name, context, bodies, params)
	return dust.helpers[name](this, context, bodies, params)
end

Chunk.prototype.capture = function(body, context, callback) 
	return this.map( 
		function(chunk) 
			local stub = Stub.new( 
			   	function(err, out) 
					if (err) then
					    chunk:setError( err )
					else
						callback(out, chunk)
					end
				end )
			
				body(stub.head, context).finish();
			end )
end

function Chunk.prototype:setError( err )
	self.error = err
	self.root.flush( self )
	return self
end

return Chunk