local dust = require "dust.helpers"
local instanceof = dust.instanceof
local Stack = require "dust.Stack"

local insert = table.insert

local Context = {}
Context.prototype = {}

function Context.new(stack, global, blocks)
    local this = {
        stack = stack,
        global = global,
        blocks = blocks,
        testvarexists = true
    }

    local echo = function( _phrase )
        return 'echo>> ' .. _phrase .. '   variable read -=> ' .. tostring( this.testvarexists )
    end

    return setmetatable(this, { __index = Context.prototype } )
    
    --[[
    return { 
        echo = echo, 
        setmetatable(this, { __index = Context.prototype } )
    } 
    ]]--
end

Context.wrap = function(context)
    print( 'Context.wrap() -=> instanceof Context -=> ' .. tostring( instanceof( context, Context ) ) )
    if (instanceof(context, Context)) then
	     return context
    else
	   return Context.new( Stack.new( context ) )
    end
end

Context.prototype.get = function(this, key) 
    print( ' in Context.get() ' )
    print( 'Context.get -=> Stack Handle -=> ' .. tostring( this.testvarexists ) )
  local ctx = this.stack
  local value = nil
    
  while(ctx) do
	  if (getmetatable(ctx) ~= nil) then
	    value = ctx.head[key]
	    if not (value == nil) then
		return value
	    end
	end
	ctx = ctx.tail
    end
    if this.global then
	return this.global[key]
    else
	return nil
    end
end

Context.prototype.getPath = function(cur, down)
  local ctx = this.stack 
  local len = #down

  if ( cur and len == 0 ) then return ctx.head end
  if ( not ctx.isObject ) then return nil end
  
  ctx = ctx.head
  local i = 0
  while( ctx and i < len) do
    ctx = ctx[down[i]];
    i = i+1
  end
  return ctx
end

Context.prototype.push = function(head, idx, len)
  return Context.new( Stack.new(head, this.stack, idx, len), this.global, this.blocks)
end

Context.prototype.rebase = function(this, head)
    return Context.new( Stack.new(head), this.global, this.blocks )
end

Context.prototype.current = function()
  return this.stack.head
end

Context.prototype.getBlock = function(key)
  local blocks = this.blocks

  if ( not blocks ) then return end
  
  local len = #blocks
  local fn = nil

  while ( true ) do
    len = len - 1
    if ( len < 1 ) then return end

    fn = blocks[len][key];
    if (fn) then return fn end
  end
end

Context.prototype.shiftBlocks = function(this, locals)
    local blocks = this.blocks

    if ( locals ) then
	     local newBlocks
	     if ( not blocks) then
	       newBlocks = { locals }
	     else
	       -- so hacky...
	       newBlocks = {}
	       for _, v in ipairs(blocks) do
		        insert(newBlocks, v)
	       end
	       insert(newBlocks, locals)
	     end
	     return Context.new(this.stack, this.global, newBlocks)
    end
    return this
end

return Context