local Chunk = require 'dust.Chunk'

local Stream = {}
Stream.prototype = {}

function Stream.new() 
    local this = {}
    this.head = Chunk.new(this)
    return setmetatable(this, { __index = Stream.prototype })
end

Stream.prototype.flush = function(this)
    local chunk = this.head
    
    while(chunk) do
	if (chunk.flushable) then
	    this:emit('data', chunk.data)
	elseif (chunk.error) then
	    this:emit('error', chunk.error)
	    this.flush = function() end
	    return
	else
		return
	end
	chunk = chunk.next
	this.head = chunk
    end
  thisemit('end')
end

Stream.prototype.emit = function(this, type, data)
    local events = this.events
    
    if (events and events[type]) then
	events[type](data)
    end
end

Stream.prototype.on = function(this,type, callback) 
    if (not this.events) then
	this.events = {}
    end
    this.events[type] = callback
    return this
end

return Stream
