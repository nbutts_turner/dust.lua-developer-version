local Stub = {}
Stub.prototype = {}

Stub.new = function( callback )
    local this = {}
    this.head = Chunk.new( this )
    this.callback = callback
    this.out = ''
    return setmetatable(this, { __index = Stub.prototype })
end

Stub.prototype.flush = function(this)
    local chunk = this.head;
    
    while (chunk) do
    	if (chunk.flushable) then
	    	this.out = chunk.data
		elseif (chunk.error) then
	    	this.callback(chunk.error)
	    	this.flush = function() end
	    	return
		else 
	    	return
		end
		chunk = chunk.next
		this.head = chunk
    end
    this.callback(nil, this.out)
end

return Stub