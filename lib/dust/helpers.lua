local dust = {}


--[[
dust.isArray = function(arr) 
    -- what about "hashes"?
    return type(arr) == "table"
end
]]--

---Checks if a table is used as an array. That is: the keys start with one and are sequential numbers
-- @param t table
-- @return nil,error string if t is not a table
-- @return true/false if t is an array/isn't an array
-- NOTE: it returns true for an empty table
-- From: http://stackoverflow.com/questions/7526223/how-do-i-know-if-a-table-is-an-array
dust.isArray = function(t)
    if type(t)~="table" then return nil,"Argument is not a table! It is: "..type(t) end
    --check if all the table keys are numerical and count their number
    local count=0
    for k,v in pairs(t) do
        if type(k)~="number" then print(v) return false else count=count+1 end
    end
    --all keys are numerical. now let's see if they are sequential and start with 1
    for i=1,count do
        --Hint: the VALUE might be "nil", in that case "not t[i]" isn't enough, that's why we check the type
        if not t[i] and type(t[i])~="nil" then return false end
    end

    return true
end


dust.isEmpty = function(value) 
    if (dust.isArray(value) and #value == 0) then return true end
    if (tonumber(value) == 0) then return false end
    
    return not value
end


dust.filter = function(_string, auto, filters) 
    if (filters) then
    	for i=1, #filters do
    	    local name = filters[i]
    	    if (tostring(name) == "s") then
    		  auto = nil
    	    else
    		  string = dust.filters[name](_string)
    	    end
    	end
    end
    
    if (auto) then
        _string = dust.filters[auto](_string)
    end
    
    return _string
end

dust.instanceof = function(elem, class)
    return getmetatable(elem) == class.prototype
end

dust.filters = {}
dust.filters["u"] = encodeURI
dust.filters["uc"] = encodeURIComponent

dust.filters["h"] = function( value ) 
  return dust.escapeHtml(value)
end

dust.filters["j"] = function( value ) 
  return dust.escapeJs(value)
end

dust.makeBase = function( global )
  return --[[new Context(new Stack(), global) ]]
end

local HCHARS = '[&<>\"]'
dust.escapeHtml = function( s ) 
  if ( type( s ) == 'string' ) then
    if ( string.find( s, HCHARS ) == nil ) then return s
    else 
      return string.gsub( s, HCHARS, 
        function( h ) 
          if ( h == '&' ) then return '&amp;'
          elseif (h == '<' ) then return '&lt'
          elseif (h == '>' ) then return '&gt;'
          elseif ( h == '"' ) then return '&quot;'
          else return h
          end
        end )
    end
  end

  return s
end

dust.escapeJs = function( s ) 
  if ( type( s ) == 'string' ) then
    return string.gsub( s, '\\', '\\' ).gsub( s, '\r', '\\r' ).gsub( s, '"', '\\"' ).gsub( s, "'", "\\'") -- .gsub( s, '\u2028', '\\u2028' ).gsub( s, '\u2029', '\\u2029' ).gsub( s, '\n', '\\n' ).gsub( s, '\f', '\\f' ).gsub( s, '\t', '\\t' )
  else
    return s
  end
end

dust.helpers = {
  sep=function(chunk, context, bodies)
    if (context.getStack().index == context.getStack().of - 1) then
      return chunk
    end
    return bodies.block(chunk, context)
  end,

  idx=function(chunk, context, bodies)
    return bodies.block(chunk, context.push(context.stack.index))
  end
}

return dust