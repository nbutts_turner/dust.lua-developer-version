local Tap = {}
Tap.prototype = {}

function Tap.new(head, tail)
    local this = {}
    this.head = head
    this.tail = tail
    return setmetatable(this, { __index = Tap.prototype})
end

Tap.prototype.push = function(this, tap)
    return Tap.new(tap, this)
end

Tap.prototype.go = function(this, value)
    local tap = this

    while(tap) do
	value = tap.head(value)
	tap = tap.tail
    end
    return value
end

return Tap