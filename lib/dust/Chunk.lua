-- local dust = require "dust.helpers"
local helpers = require "dust.helpers"
local instanceof = helpers.instanceof
local Tap = require "dust.Tap"
local Stub = require 'dust.stub'

local Chunk = {}
Chunk.prototype = {}

Chunk.new = function( root, _next, taps )
    local this = {
		root = root,
		next = _next,
		data = '',
		flushable = false,
		taps = taps
    }
    return setmetatable( this, { __index = Chunk.prototype } )
end

function Chunk.prototype:write( data )
    local taps = self.taps
    
    if (taps) then data = taps.go(data) end
    self.data = self.data .. tostring( data )
    return self
end

Chunk.prototype.finish = function(this, data) 
	if (data) then
    	this:write(data)
    end
    this.flushable = true
    this.root:flush( this.root )
    return this
end

Chunk.prototype.map = function(this, callback) 
	local cursor = Chunk.new(this.root, this.next, this.taps)
    local branch = Chunk.new(this.root, cursor, this.taps)
    
    this.next = branch
    this.flushable = true
    callback(branch)
    return cursor
end

function Chunk.prototype:tap( tap )
    local taps = self.taps
    
    if (taps) then
    	this.taps = taps:push(tap)
    else 
    	self.taps = Tap.new( tap )
    end
    return self
end

function Chunk.prototype:untap()
	self.taps = self.taps.getTail()
    return self
end

function Chunk.prototype:render( body, context )
    return body(self, context)
end

function Chunk.prototype:reference( elem, context, auto, filters ) 
    if ( type(elem) == "function" ) then
    	elem = elem( self, context, nil, {auto = auto, filters = filters} )
		if ( instanceof(elem, Chunk)) then
	    	return elem
		end
    end
    if (not helpers.isEmpty(elem)) then
    	if ( auto == '' ) then auto = nil end
		return self:write( helpers.filter(elem, auto, filters) )
    else
		return self
    end
end

function Chunk.prototype:section( elem, context, bodies, params ) 
	if (type(elem) == "function") then
    	elem = elem(self, context, bodies, params)
		if (instanceof(elem, Chunk)) then
	    	return elem
		end
    end

    local body = bodies.block
    local skip = bodies['otherwise']

    if ( params ) then
    	context = context.push( params )
	end

	if ( helpers.isArray(elem) ) then
		if (body) then
			local len = #elem
			local chunk = self
			if ( len > 0 ) then
				-- // any custom helper can blow up the stack 
        		-- // and store a flattened context, guard defensively
        		-- if (context.stack.head) then
         	-- 		context.stack.head['$len'] = len;
        		-- end
				for i=1, len do
					-- if ( context.stack.head ) then
     --       				context.stack.head['$idx'] = i;
     --      			end
					chunk = body(chunk, context.push(elem[i], i, len))
			    end
			    -- if (context.stack.head) then
       --   			context.stack.head['$idx'] = nil
       --   			context.stack.head['$len'] = nil
       -- 			end
				return chunk
			elseif ( skip ) then
				return skip( self, context )
			end
		end
	elseif (elem == true) then
		if (body) then return body(self, context) end
	elseif ( elem or elem == 0 ) then
		if ( body ) then return body(self, context.push(elem)) end
	elseif (skip) then return skip(self, context) 
	else end

    return self;
end

function Chunk.prototype:exists(elem, context, bodies) 
	local body = bodies.block
	local skip = bodies['otherwise']

	if (not helpers.isEmpty(elem)) then
	    if (body) then return body(self, context) end
	elseif (skip) then return skip(self, context) end
		
	return self
end

function Chunk.prototype:notexists(elem, context, bodies)
	local body = bodies.block
	local skip = bodies['otherwise']

	if ( helpers.isEmpty(elem) ) then
	    if (body) then return body(self, context) end
	elseif (skip) then 
		return skip(self, context) 
	end
		
	return self
end

function Chunk.prototype:block(elem, context, bodies) 
	local body = bodies.block
	
	if (elem) then body = elem end
	if (body) then return body(self, context) end
	return self
end

function Chunk.prototype:partial(elem, context)
	print( 'Chunk.prototype:partial' )
	if ( type( elem ) == "function" ) then
		return self:capture(elem, context, 
				function(name, chunk) 
					local chunklocal = dust.load(name, chunk, context)
					chunklocal.finish()
				end )
	end
	
	return dust.load(elem, self, context)
end

function Chunk.prototype:helper(name, context, bodies, params)
	if ( helpers.helpers ) then
		return helpers.helpers[name](self, context, bodies, params)
	end
end

function Chunk.prototype:capture(body, context, callback) 
	return self.map( self,
		function(chunk) 
			local stub = Stub.new( 
			   	function(err, out) 
					if (err) then
					    chunk:setError( err )
					else
						callback(out, chunk)
					end
				end )
			
				local chunklocal = body(stub.head, context)
				chunklocal.finish( self );
			end )
end

function Chunk.prototype:setError( err )
	self.error = err
	self.root.flush( self )
	return self
end

return Chunk