local dust = require "dust.helpers"
local instanceof = dust.instanceof
local Stack = require "dust.Stack"

local insert = table.insert

local Context = {}
Context.prototype = {}

function Context.new(stack, global, blocks)
    local this = {
        stack = stack,
        global = global,
        blocks = newBlocks
    }

    local get = function( key )
        local ctx = this.stack
        local value = nil

        while( ctx ) do
            if ( getmetatable(ctx) ~= nil ) then
                if ( ctx.isObject ) then
                    value = ctx.head[key]
                    if not (value == nil) then
                        return value
                    end
                end
            end
            
            ctx = ctx.tail
        end
        
        if this.global then
            return this.global[key]
        else
            return nil
        end
    end

    local getPath = function( cur, down )
        local ctx = this.stack 
        local len = #down

        if ( cur and len == 0 ) then return ctx.head end

        if ( not ctx.isObject ) then return nil end

        ctx = ctx.head
        local i = 1
        while( ctx and i <= len) do
            ctx = ctx[down[i]];
            i = i+1
        end
        return ctx
    end

    local push = function(head, idx, len)
       return Context.new( Stack.new(head, this.stack, idx, len), this.global, this.blocks)
    end

    local rebase = function( head )
        return Context.new( Stack.new(head), this.global, this.blocks )
    end

    local current = function()
        return this.stack.head
    end

    local getBlock = function(key)
        local blocks = this.blocks

        if ( not blocks ) then return end
  
        local len = #blocks
        local fn = nil

        while ( true ) do
            len = len - 1
            if ( len < 1 ) then return end

            fn = blocks[len][key];
            if (fn) then return fn end
         end
    end

    local shiftBlocks = function(this, locals)
        local blocks = this.blocks

        if ( locals ) then
             local newBlocks
             if ( not blocks) then
               newBlocks = { locals }
             else
               -- so hacky...
               newBlocks = {}
               for _, v in ipairs(blocks) do
                    insert(newBlocks, v)
               end
               insert(newBlocks, locals)
             end
             return Context.new(this.stack, this.global, newBlocks)
        end
        return this
    end

    local getStack = function()
         return this.stack
    end

    return { 
        get = get,
        getPath = getPath,
        push = push,
        rebase = rebase,
        current = current,
        getBlock = getBlock,
        shiftBlocks = shiftBlocks,
        getStack = getStack
    } 
    
end



Context.wrap = function(context)
    if (instanceof(context, Context)) then
	     return context
    else
	   return Context.new( Stack.new( context ) )
    end
end



return Context