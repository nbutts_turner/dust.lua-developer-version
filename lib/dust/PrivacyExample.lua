Test_Type = {};

function Test_Type.__len (o)
   return 1;
end;

function new_test ()
   local result = {a = 'hello' };
   local printResult = function() 
   	print( result.a )
   end

   setmetatable(result, Test_Type);
   return result;
end;

do
   local a_test = new_test();
   print (#a_test);
   print(getmetatable(a_test).__len(a_test));
   print( a_test.a )
end;