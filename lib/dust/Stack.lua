local Stack = {}
Stack.prototype = {}

Stack.new = function( head, tail, idx, len )
	local this = {
		head = head,
		tail = tail,
		isObject = not dust.isArray( head ) and head and type(head) == 'table',
		index = idx,
		of = len
	}
	return setmetatable( this, { __index = Stack.prototype } )
end 

return Stack