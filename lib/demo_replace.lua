dust = require 'dust'
local M = {}
function M.register()
 dust.register( "replace",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:write( "Hello "):reference(ctx.get( "name" ),ctx,"h"):write( "! You have "):reference(ctx.get( "count" ),ctx,"h"):write( " new messages.")
end
 
return M
