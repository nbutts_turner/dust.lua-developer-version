dust = require 'dust'
local M = {}
function M.register()
 dust.register( "else_block",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx.get( "foo" ),ctx,{otherwise=M.body_1,block=M.body_2},nil):section(ctx.get( "bar" ),ctx,{otherwise=M.body_3,block=M.body_4},nil)
end
 
function M.body_1(chk,ctx)
return chk:write( "not foo, ")
end
 
function M.body_2(chk,ctx)
return chk:write( "foo, ")
end
 
function M.body_3(chk,ctx)
return chk:write( "not bar!")
end
 
function M.body_4(chk,ctx)
return chk:write( "bar!")
end
 
return M
