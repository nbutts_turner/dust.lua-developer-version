dust = require 'dust'
local M = {}
function M.register()
 dust.register( "base_template",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:write( "Start\n"):block(ctx:getBlock("title"),ctx,{block=M.body_2},nil):write( "\n"):block(ctx:getBlock("main"),ctx,{block=M.body_4},nil):write( "\n End")
end
 
function M.body_1(chk,ctx)
return chk:write( " Base Title ")
end
 
function M.body_2(chk,ctx)
return chk:write( " Base Title ")
end
 
function M.body_3(chk,ctx)
return chk:write( " Base Content ")
end
 
function M.body_4(chk,ctx)
return chk:write( " Base Content ")
end
 
return M
