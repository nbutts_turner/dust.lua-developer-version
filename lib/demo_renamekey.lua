dust = require 'dust'
local M = {}
function M.register()
 dust.register( "rename_key",M.body_0); 
end
function M.body_0(chk,ctx)
return chk:section(ctx.get( "person" ),ctx,{block=M.body_1},{foo=ctx.get( "root" )})
end
 
function M.body_1(chk,ctx)
return chk:reference(ctx.get( "foo" ),ctx,"h"):write( ": "):reference(ctx.get( "name" ),ctx,"h"):write( ", "):reference(ctx.get( "age" ),ctx,"h")
end
 
return M
