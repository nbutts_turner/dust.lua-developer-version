#!/usr/bin/env luajit

dust = require 'dust'
Chunk = require 'dust.Chunk'
cron = require 'cron.cron'

local template = require 'demo_array'
local intro_template = require 'demo_intro'
local combo_template = require 'demo_combo'
local plain_template = require 'demo_plain'
local replaceTemplate = require 'demo_replace'
local implicit_template = require 'demo_implicit'
local object_template = require 'demo_object'
local path_template = require 'demo_path'
local renamekey_template = require 'demo_renamekey'
local forcecurrent_template = require 'demo_forcecurrent'
local escaped_template = require 'demo_escaped'
local escapedpragma_template = require 'demo_escapedpragma'
local else_template = require 'demo_else'
local conditional_template = require 'demo_conditional'
local synckey_template = require 'demo_synckey'
local syncchunk_template = require 'demo_syncchunk'
local asynciterator_template = require 'demo_asynciterator'
local filter_template = require 'demo_filter'
local context_template = require 'demo_context'
local param_template = require 'demo_param'
local partials_template = require 'demo_partials'
local partialcontext_template = require 'demo_partialcontext'
local base_template = require 'demo_basetemplate'
local child_template = require 'demo_childtemplate'
local recursion_template = require 'demo_recursion'
local comment_template = require 'demo_comment'

intro_template.register()
template.register()
replaceTemplate.register()

-- intro_template block
print( "\n\nINTRO\n__________________\n" )
dust.renderDemo( 'intro', function() 
  local d = 1
  return {
    stream=function()
      return "asynchronous templates for the browser and node.js"
    end,
    delay=function(chunk, context, bodies)
      		return chunk.map( chunk, 
      			function(chunk) 
      				local rval = chunk:render(bodies.block, context)
      				rval.finish( rval ) 
      			end, 
      			(d+1) * 15 )
      	  end 
  }
end, 
function( _ref, _data ) print( '==============================================================\nINTRO\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- combo template block
print( "\n\nCOMBO\n__________________\n" )
combo_template.register()
dust.render( 'combo', {title="Sir", names={{name="Moe"}, {name="Larry"}, {name="Curly"}}}, 
  function( _ref, _data ) print( '==============================================================\nCOMBO\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- plain template block
print( "\n\nPLAIN\n__________________\n" )
plain_template.register()
dust.render( 'plain', {}, 
  function( _ref, _data ) print( '==============================================================\nPLAIN\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- replace template block
print( "\n\nREPLACE\n__________________\n" )
 dust.render( 'replace', { name='mick', count=30 }, 
  function( _ref, _data ) print( '==============================================================\nREPLACE\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- arrary template block
print( "\n\nARRAY\n__________________\n" )
 dust.render( 'array', {title="Sir", names={{name="Moe"}, {name="Larry"}, {name="Curly"}}}, 
  function( _ref, _data ) print( '==============================================================\nARRAY\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- implicit template block
print( "\n\nIMPLICIT\n__________________\n" )
implicit_template.register()
dust.render( 'implicit',  { names={"Moe", "Larry", "Curly"} }, 
  function( _ref, _data ) print( '==============================================================\nIMPLICIT\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- object template block
print( "\n\nOBJECT\n__________________\n" )
object_template.register()
dust.render( 'object', { root="Subject", person={name="Larry",age=45} }, 
  function( _ref, _data ) print( '==============================================================\nOBJECT\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- path template block
print( "\n\nPATH\n__________________\n" )
path_template.register()
dust.render( 'path', { foo={ bar="Hello!" } }, 
  function( _ref, _data ) print( '==============================================================\nPATH\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- rename Key
print( "\n\nRENAME\n__________________\n" )
renamekey_template.register()
dust.render( 'rename_key', { root="Subject", person={ name="Larry", age=45 } }, 
  function( _ref, _data ) print( '==============================================================\nRENAME\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- force_current
print( "\n\nFORCE CURRENT\n__________________\n" )
forcecurrent_template.register()
dust.render( 'force_current', { root="Subject", person={name="Larry",age=45 } }, 
  function( _ref, _data ) print( '==============================================================\nFORCE CURRENT\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- escaped
print( "\n\nESCAPED\n__________________\n" )
escaped_template.register()
dust.render( 'escaped', {safe="<script>alert('Hello!')</script>", unsafe="<script>alert('Goodbye!')</script>"}, 
  function( _ref, _data ) print( '==============================================================\nESCAPED\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- escaped pragma
print( "\n\nESCAPED PRAGMA\n__________________\n" )
escapedpragma_template.register()
dust.render( 'escaped_pragma', {unsafe="<script>alert('Goodbye!')</script>"}, 
  function( _ref, _data ) print( '==============================================================\nESCAPED PRAGMA\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- else
print( "\n\nELSE\n__________________\n" )
else_template.register()
dust.render( 'else_block', {foo=true,bar=false}, 
  function( _ref, _data ) print( '==============================================================\nELSE\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- conditional
print( "\n\nCONDITIONAL\n__________________\n" )
conditional_template.register()
dust.render( 'conditional', {tags={},likes={"moe", "larry", "curly", "shemp"}}, 
  function( _ref, _data ) print( '==============================================================\nCONDITIONAL\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- sync_key
print( "\n\nSYNC KEY\n__________________\n" )
synckey_template.register()
dust.render( 'sync_key', {type=function(chunk) return "Sync" end }, 
  function( _ref, _data ) print( '==============================================================\nSYNC KEY\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- sync_chunk
print( "\n\nSYNC CHUNK\n__________________\n" )
syncchunk_template.register()
dust.render( 'sync_chunk', {type=function(chunk) return chunk:write("Chunky") end }, 
  function( _ref, _data ) print( '==============================================================\nSYNC CHUNK\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )

-- async_iterator
print( "\n\nASYNC ITERATOR\n__________________\n" )
asynciterator_template.register()
dust.render( 'async_iterator', { numbers={3, 2, 1}, 
  delay=function(chunk, context, bodies) 
    return chunk.map( chunk, 
      function(chunk)  
          cron.after( 10, 
            function() 
              local c = chunk:render( bodies.block, context )
              c.finish( c ) 
            end
          ) 
          cron.update(10)
        end
      )
    end 
  }, 
  function( _ref, _data ) print( '==============================================================\nASYNC ITERATOR\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end 
) -- Need to implement a random number generator for the Timeout function (Math.ceil(Math.random() * 10)  )


-- filter
print( "\n\nFILTER\n__________________\n" )
filter_template.register()
dust.render( 'filter', { 
    filter=function(chunk, context, bodies) 
      return chunk:tap( function(data) return string.upper( data ) end ):render(bodies.block, context):untap()
    end, bar="bar"
  }, 
  function( _ref, _data ) print( '==============================================================\nFILTER\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end
) -- Need to implement a random number generator for the Timeout function (Math.ceil(Math.random() * 10)  )


-- context
print( "\n\nCONTEXT\n__________________\n" )
context_template.register()
dust.render( 'context', {
  list=function(chunk, context, bodies)
    local items = context.current()
    local len = items and #items
    print( "length of items: " .. tostring(len) )
    if (len) then
      chunk:write("<ul>\n")
      for i=0, len do
        chunk = chunk:write("<li>"):render(bodies.block, context.push(items[i])):write("</li>\n");
      end
      return chunk:write("</ul>")
    elseif (bodies['otherwise']) then
      return chunk:render(bodies['otherwise'], context)
    end
    return chunk;
  end,
  projects={ {name="Mayhem"}, {name="Flash"}, {name="Thunder"} }
  }, 
  function( _ref, _data ) print( '==============================================================\nCONTEXT\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end
)



-- param
print( "\n\nPARAM\n__________________\n" )
param_template.register()
dust.render( 'param', {
  helper=function(chunk, context, bodies, params)
    return chunk:write(params.foo)
  end
}, 
  function( _ref, _data ) print( '==============================================================\nPARAM\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- partials
print( "\n\nPARTIALS\n__________________\n" )
partials_template.register()
dust.render( 'partials', {name="Jim",count=42,ref="plain"}, 
  function( _ref, _data ) print( '==============================================================\nPARTIALS\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end 
)


-- partial context
partialcontext_template.register()
dust.render( 'partial_context', { profile={ name="Mick", count=30 } }, 
  function( _ref, _data ) print( '==============================================================\nPARTIAL CONTENT\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )



-- base template
base_template.register()
dust.render( 'base_template', { }, 
  function( _ref, _data ) print( '==============================================================\nBASE TEMPLATE\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- child template
child_template.register()
dust.render( 'child_template', {}, 
  function( _ref, _data ) print( '==============================================================\nCHILD TEMPLATE\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- recursion
recursion_template.register()
dust.render( 'recursion', { name="1", kids={ { name="1.1", kids={ { name="1.1.1" } } } } }, 
  function( _ref, _data ) print( '==============================================================\nRECURSION\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


-- conmment
comment_template.register()
dust.render( 'comment', { }, 
  function( _ref, _data ) print( '==============================================================\nCOMMENT\n______________________________________________________________\n' .. tostring( _data ) .. '\n==============================================================\n' ) end )


print( '\n\nEND DUST_START.LUA\n__________________' )  

